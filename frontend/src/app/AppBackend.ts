import LobbyService from "./lobby/LobbyService";
import RestClient from "./AppUtil";
import GameService from "./lobby/GameService";
import {appNavigator} from "./AppNavigator";

export default class AppBackend{

    lobby : LobbyService;
    game : GameService;

    constructor(lobby:LobbyService, game:GameService) {
        this.lobby = lobby;
        this.game = game;
    }

    public static async create(url:string):Promise<AppBackend>{
        let restClient = await RestClient.create(url)
        return new AppBackend(
            new LobbyService(restClient),
            new GameService(restClient)
        );
    }
}

export var appBackend:AppBackend = null;
AppBackend.create("http://localhost:8080").then((a)=> {
    appBackend = a;
    appNavigator.lobby();
});