console.log(`
 AppUtil.ts Version 0.1.1 2022-01-22
  - AppNavigatorBase
  - RestClient
`);

export default class RestClient {
    url:string;
    private token: string = null;


    constructor(url:string){
        this.url = url;
    }

    async getHeaders(){
        let headers:Headers = new Headers();
        if(this.token != null){
            headers.set('Authorization', 'Bearer ' + this.token);
        }
        headers.set('Content-Type', 'application/json');
        return headers;
    }

    public static async create(url:string){
        let rc = new RestClient(url);
        return rc;
    }

    async post(uri:string, obj:any){
        return await fetch(this.url + uri, {
            method: 'POST',
            headers: await this.getHeaders(),
            body: JSON.stringify(obj)
        }).then((f)=>RestClient.handleResponse(f, this.url + uri))
            .catch(e=>RestClient.handleError(e, this.url + uri, "post", obj));
    }


    async get(uri:string):Promise<any>{
        return fetch(this.url + uri, {method:"GET", headers: await this.getHeaders()})
            .then((f)=>RestClient.handleResponse(f, this.url + uri))
            .catch(e=>RestClient.handleError(e, this.url + uri, "get"));
    }

    async delete(uri:string){
        await fetch(this.url + uri, {
            method: 'DELETE',
            headers: await this.getHeaders()
        })
            .then((f)=>RestClient.handleResponse(f, this.url + uri))
            .catch(e=>RestClient.handleError(e, this.url + uri, "delete"));
    }

    async put(uri:string, obj:any):Promise<any>{
        return await fetch(this.url + uri, {
            method: 'PUT',
            headers: await this.getHeaders(),
            body: JSON.stringify(obj)
        })
            .then((f)=> {
                if(f.status == 200){
                    return f.json();
                }else{
                    RestClient.handleResponse(f,this.url + uri)
                }
            })
            .catch(e=>RestClient.handleError(e, this.url + uri, "put", obj));
    }

    public static async handleResponse(res: Response, url:string) {
        function prop(key, value){
            return key + ": " + value + "\n";
        }
        function text(text){
            return text+"\n\n";
        }
        let status = res.status;
        if(status == 200){
            return res.json();
        } else if(status == 204){
            return null;
        }
        else {
            let message = text("Status Text: "+res.statusText)
                +prop("http statusCode", status)
                +prop("url", url);
            console.log("Look at XHR response for details");
            console.log("Unexpected Response: AppUtil.ts in line:")
            console.log(res);
            console.log(message);
            alert(
                text("Unexpected Response: Details in console.log")
                +message
            );
            return Promise.reject(res.text());
        }
    }

    public static handleError(e:any, url:string, methodName:string, data = null){
        function prop(key, value){
            return key + ": " + value + "\n";
        }

        function text(text){
            return text+"\n\n";
        }

        let message = text("Error: "+e.error)
            +prop("message", e.message)
            +prop("methodName", methodName)
            +prop("url", url)
            +prop("data", JSON.stringify(data))
            +prop("trace", e.trace);

        console.log("Error: AppUtil.ts in line:")
        console.log(e);
        console.log(e.trace);
        console.log(message);

        alert(
            message
            + text("\nError: Details in console.log")
        );

        return Promise.reject(e.message);
    }

    public setToken(token: any) {
        this.token = token;
    }

    public hasToken() {
        return this.token != null;
    }
}

/**
 * export class AppNavigator extends AppNavigatorBase{
 *     public lobby = () => this.navigate("lobby");
 *     public isLobby = () => this.isPage("lobby");
 * }
 *
 * export const appNavigator = new AppNavigator();
 */

export class AppNavigatorBase{

    page:string = null;
    //_pageStore = writable("");
    listeners = [];

    constructor(){
        //this._pageStore.subscribe((val)=>this.page = val);
    }

    public home(){
        this.navigate("/");
    }

    public isHome():boolean{
        return this.isPage("/");
    }

    /**
     *
     *  if(appNavigator.isDetails()){
     *      let id = navigator.getParam("id");
     *  }
     *
     * @param name
     */
    public getParam(name:string):string{
        try{
            let paramPart = this.page.split("?")[1];
            let na = "&"+paramPart+"&";
            let valueStart = na.split("&"+name+"=")[1];
            let value = valueStart.split("&")[0];
            return value;
        }catch(e){
            throw new Error("Param "+name+" not found. On: "+this.page+"."+e);
        }
    }

    /**
     *     public details(id:number){
     *         this.navigate(Urls.details + "?id="+id);
     *     }
     *
     * @param url
     * @private
     */
    protected navigate(url:string){
        console.log("navigate to: "+ url);
        this.page = url;
        this.listeners.forEach((l)=>l());
        //this._pageStore.set(url);
        //location.href = url;
    }
    protected isPage(url:string):boolean{
        return (this.page.split("?")[0]) === url;
    }

    /**
     * Listen to changes:
     *
     * let navigate = appNavigator;
     * appNavigator.onChange(()=> navigate = navigate);
     *
     * @param fun
     */
    public onChange(fun: () => void){
        //this._pageStore.subscribe(fun);
        this.listeners.push(fun);
    }

    public isReady() : boolean{
        return this.page != null;
    }
}


