package de.code.cloud95.spygame.page.entity;

import de.code.cloud95.spygame.page.boundary.Page;
import lombok.RequiredArgsConstructor;

import javax.ws.rs.core.MultivaluedMap;

@RequiredArgsConstructor
public class Template {
    private static final String LANG = "de";
    private static final String TITLE = "Titel";

    public String render(Page content, MultivaluedMap<String, String> params) {

        return  "<!DOCTYPE html>\n" +
                "<html lang=\""+LANG+"\">\n" +
                "  <head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <title>"+TITLE+"</title>\n" +
                "  </head>\n" +
                "  <body>\n" +
                content.render(params) +
                "  </body>\n" +
                "</html>";
    }
}
