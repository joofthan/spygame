package de.code.cloud95.spygame.page.entity;

import de.code.cloud95.spygame.page.boundary.Page;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Link {

    public static <T extends Page> String to(Class<T> page, String text){
        return "<a href=\""+page.getSimpleName()+"\">"+text+"</a>";
    }

    public static <T extends Page> String to(Class<T> page){
        return to(page, page.getSimpleName().replaceAll("Page", ""));
    }
}
