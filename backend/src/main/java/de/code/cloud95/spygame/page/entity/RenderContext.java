package de.code.cloud95.spygame.page.entity;

public final class RenderContext {
    private final String indent;

    public RenderContext(
            String indent
    ) {
        this.indent = indent;
    }

    public String line(String... lines) {
        String content = "";
        for (String line : lines) {
            content = content + indent + line + "\n";
        }
        return content;
    }

    public RenderContext addIndent(String text) {
        return new RenderContext(this.indent + text);
    }

    public String indent() {
        return indent;
    }

    @java.lang.Override
    public boolean equals(java.lang.Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (RenderContext) obj;
        return java.util.Objects.equals(this.indent, that.indent);
    }

    @java.lang.Override
    public int hashCode() {
        return java.util.Objects.hash(indent);
    }

    @java.lang.Override
    public String toString() {
        return "RenderContext[" +
                "indent=" + indent + ']';
    }

}
