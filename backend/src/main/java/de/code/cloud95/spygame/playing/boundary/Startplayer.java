package de.code.cloud95.spygame.playing.boundary;

import de.code.cloud95.spygame.StatelessBoundary;
import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.entity.Game;
import de.code.cloud95.spygame.playing.entity.Player;
import de.code.cloud95.spygame.lobby.boundary.Lobby;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Duration;
import java.util.List;

@StatelessBoundary
@NoArgsConstructor(force = true)
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class Startplayer {
    final UserService userService;
    final EntityManager em;
    final Lobby lobby;
    final Decks decks;


    public void startNewGame(int rounds, int minutes) {
        List<Player> waitingPlayers = lobby.waiting();
        var newGame = new Game(rounds, Duration.ofMinutes(minutes), waitingPlayers, userService.getUser().id);


        newGame = em.merge(newGame);
        for (var p: waitingPlayers) {
            p.gameId = newGame.id;
        }
    }

    public void chooseStartDeck(Long deckId) {
        if(userService.getUser().gameId == null){
            throw new RuntimeException("Player has no game");
        }

        var game = em.find(Game.class, userService.getUser().gameId);

        var players  = em.createNamedQuery(Player.BY_GAME_ID, Player.class).setParameter("gameId", userService.getUser().gameId).getResultList();
        var deck = decks.findById(deckId);
        for (var p: players){
            p.cardId(deck.draw().cardId);
            em.merge(p);
        }


        var round = game.currentRound();
        round.setDeck(deckId);
        em.merge(round);
    }
}
