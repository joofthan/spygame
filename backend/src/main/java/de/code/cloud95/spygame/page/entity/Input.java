package de.code.cloud95.spygame.page.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Input implements Renderable {
    private final String name;
    @Override
    public String render() {
        return "<input name=\""+name+"\" type=\"text\">";
    }
}
