package de.code.cloud95.spygame.page.boundary;

import de.code.cloud95.spygame.page.entity.Renderable;

import javax.ws.rs.core.MultivaluedMap;

public interface Page extends Renderable {
    String render(MultivaluedMap<String, String> params);

    default String render(){
        return render();
    }
}
