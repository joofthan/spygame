package de.code.cloud95.spygame.authentication.control;

import de.code.cloud95.spygame.playing.entity.Player;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import static java.lang.Long.parseLong;

@RequestScoped
@NoArgsConstructor(force = true)
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class UserService {
    final TokenGenerator tg;
    final EntityManager em;

    public Player getUser(){
        return em.find(Player.class, tg.getPlayerId());
    }

    public String addUser(String name) {
        var p = em.merge(new Player(name));
        return tg.generateToken(name, p.id);
    }
}
