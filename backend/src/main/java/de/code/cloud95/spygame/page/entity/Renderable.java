package de.code.cloud95.spygame.page.entity;

public interface Renderable {
    String render();

    default String render(Renderable...renderables){
        String content = "";
        for(Renderable r:renderables){
            content = content + r.render();
        }

        return content;
    }
}
