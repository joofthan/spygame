package de.code.cloud95.spygame.lobby.boundary;

import de.code.cloud95.spygame.StatelessBoundary;
import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.entity.Player;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@StatelessBoundary
@NoArgsConstructor(force = true)
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class Lobby {
    final UserService userService;
    final EntityManager em;

    public String join(String name){
        return userService.addUser(name);
    }

    public List<Player> waiting() {
        return em.createQuery(Player.WAITING, Player.class).getResultList();
    }
}
