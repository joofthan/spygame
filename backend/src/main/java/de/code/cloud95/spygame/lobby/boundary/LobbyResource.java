package de.code.cloud95.spygame.lobby.boundary;

import de.code.cloud95.spygame.StatelessBoundary;
import de.code.cloud95.spygame.authentication.control.TokenGenerator;
import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.boundary.Startplayer;
import de.code.cloud95.spygame.playing.entity.Player;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import java.util.List;


@StatelessBoundary
@Path("lobby")
@NoArgsConstructor(force = true)
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class LobbyResource {
    final Lobby lobby;
    final Startplayer startplayer;
    final TokenGenerator tg;
    final UserService us;

    @PUT
    @Path("join")
    public JsonObject join(UserJoinRequest user){
        return Json.createObjectBuilder()
                .add("token", us.addUser(user.name))
                .build();
    }

    @NoArgsConstructor
    @AllArgsConstructor
    public static class UserJoinRequest {
        public String name;

    }

    @GET
    public List<Player> waiting(){
        return lobby.waiting();
    }

    @SecurityRequirement(name = "apiKey")
    @POST
    @Path("start")
    public void startGame(StartGameRequest req){
        startplayer.startNewGame(req.round, req.minutes);
    }

    public static class StartGameRequest {
        public int round;
        public int minutes;
    }
}
