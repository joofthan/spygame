package de.code.cloud95.spygame.playing.boundary;

import de.code.cloud95.spygame.StatelessBoundary;
import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.entity.*;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@StatelessBoundary
@NoArgsConstructor(force = true)
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class Decks {
    final UserService userService;
    final EntityManager em;

    public List<Deck> list(){
        var gameId = userService.getUser().gameId;
        var game = em.find(Game.class, gameId);

        if(!userService.getUser().id.equals(game.startplayerId)){
            throw new PlayerIsNotStartplayerException();
        }
        return getDecks();
    }

    private List<Deck> getDecks() {
        return List.of(
                Deck.create(1L, Location.CASINO, "Barkeeper", "Dealer", "Spieler", "Croupier", "Falschspieler", "Türsteher", "Sicherheitsschef"),
                Deck.create(2L, Location.CASINO, "Barkeeper", "Dealer", "Spieler", "Croupier", "Falschspieler", "Türsteher", "Sicherheitsschef")
        );
    }


    public Card findCardById(CardId cardId) {
        return findById(cardId.deckId).cards().stream().filter(card->card.cardId.equals(cardId)).findFirst().orElseThrow(()-> new RuntimeException("Card with id "+cardId + " not found."));

    }

    public Deck findById(Long deckId) {
        return getDecks().stream().filter(deck -> deck.id().equals(deckId)).findFirst().orElseThrow(() -> new RuntimeException("Deck with id "+deckId + " not found."));
    }
}
