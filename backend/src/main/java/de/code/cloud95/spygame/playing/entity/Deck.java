package de.code.cloud95.spygame.playing.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public final class Deck {
    public final Long id;
    public final List<Card> cards;

    public Deck(
            Long id,
            List<Card> cards
    ) {
        this.id = id;
        this.cards = cards;
    }

    public static Deck create(Long id, Location location, String... names) {
        List<Card> cards = new ArrayList<>();
        int cardNr = 1;
        cards.add(new Card(new CardId(id, cardNr), Location.NONE, Card.AGENT));
        for (String name : names) {
            cardNr++;
            cards.add(new Card(new CardId(id, cardNr), location, name));
        }

        return new Deck(id, cards);
    }


    public Card draw() {
        var c = cards.get(0);
        cards.remove(0);
        return c;
    }

    public boolean contains(String job) {
        return cards.stream().anyMatch(e -> e.job.equals(job));
    }

    public Long id() {
        return id;
    }

    public List<Card> cards() {
        return cards;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Deck) obj;
        return Objects.equals(this.id, that.id) &&
                Objects.equals(this.cards, that.cards);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cards);
    }

    @Override
    public String toString() {
        return "Deck[" +
                "id=" + id + ", " +
                "cards=" + cards + ']';
    }

}
