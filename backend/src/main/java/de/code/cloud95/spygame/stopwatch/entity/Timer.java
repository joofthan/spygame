package de.code.cloud95.spygame.stopwatch.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.Duration;
import java.time.LocalDateTime;

@Entity
public class Timer {
    @Id
    public Long id;
    private LocalDateTime runningSince;
    private Duration duration;
    private boolean running;
    public Timer(){

    }
    public Timer(Long gameId){
        this.id = gameId;
        duration = Duration.ZERO;
    }


    private void start(LocalDateTime now){
        if(running){
            throw new RuntimeException("Timer is already running");
        }
        this.runningSince = now;
        this.running = true;
    }

    private void stop(LocalDateTime now){
        if(!running){
            throw new RuntimeException("Timer is not running");
        }
        this.duration.plus(Duration.between(runningSince, now));
        this.running = false;
    }

    public Duration read(LocalDateTime now) {
        if(running) {
            return duration.plus(Duration.between(runningSince, now));
        }else{
            return duration;
        }
    }

    public void startStop(LocalDateTime now) {
        if(running){
            stop(now);
        }else{
            start(now);
        }
    }
}
