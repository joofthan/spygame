package de.code.cloud95.spygame.playing.entity;

import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
public class Game {
    @Id
    @GeneratedValue
    public Long id;
    public Duration roundDuration;
    @OneToMany
    public List<Player> players;
    public Long startplayerId;
    private int currentRound;
    @OneToMany(mappedBy = "game", cascade = CascadeType.ALL)
    public List<Round> rounds;

    public Game(int rounds, Duration roundDuration, List<Player> players, Long startplayerId){
        this.roundDuration = roundDuration;
        this.players = players;
        this.startplayerId = startplayerId;
        this.rounds = new ArrayList<>();
        for (int i = 0; i <rounds; i++) {
            this.rounds.add(new Round());
        }
        this.currentRound = 0;
    }

    public Round currentRound(){
        return rounds.get(currentRound);
    }
}
