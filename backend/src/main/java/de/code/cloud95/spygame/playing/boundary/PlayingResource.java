package de.code.cloud95.spygame.playing.boundary;

import de.code.cloud95.spygame.authentication.boundary.UserResource;
import de.code.cloud95.spygame.lobby.boundary.Lobby;
import de.code.cloud95.spygame.playing.entity.Deck;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import java.util.List;

@Path("playing")
@NoArgsConstructor(force = true)
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class PlayingResource {
    final Decks decks;
    final Startplayer startplayer;
    final Lobby lobby;

    @SecurityRequirement(name = UserResource.API_KEY)
    @GET
    @Path("decks")
    public List<Deck> listAll(){
        return decks.list();
    }

    @SecurityRequirement(name = UserResource.API_KEY)
    @POST
    @Path("startdeck")
    public void choose(Long deckId){
        startplayer.chooseStartDeck(deckId);
    }
}
