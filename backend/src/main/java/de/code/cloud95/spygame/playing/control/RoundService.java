package de.code.cloud95.spygame.playing.control;

import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.entity.Round;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor(force = true)
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class RoundService {
    final UserService userService;
    final EntityManager em;

    public Round findCurrentRound(){
        var gameId = userService.getUser().gameId;
        return em.createQuery("select r from Round r where r.game.id = ?1", Round.class).setParameter(1,gameId).getSingleResult();
    }

    public List<Round> findAll() {
        return em.createQuery("select r from Round r", Round.class).getResultList();
    }
}
