package de.code.cloud95.spygame.playing.entity;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import java.util.Objects;

@EqualsAndHashCode
@ToString
@Embeddable
public class CardId {
    public Long deckId;
    public Integer cardNr;

    public CardId(){

    }

    public CardId(Long deckId, Integer cardNr){
        if(deckId == null || deckId.equals(0L)){
            throw new IllegalArgumentException("deckId is null");
        }
        if(cardNr == null || cardNr.equals(0)){
            throw new IllegalArgumentException("cardNr is null");
        }
        this.deckId = deckId;
        this.cardNr = cardNr;
    }

}
