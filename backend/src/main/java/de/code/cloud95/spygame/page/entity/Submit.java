package de.code.cloud95.spygame.page.entity;

public class Submit implements Renderable {

    private String text;

    public Submit(String text){
        this.text = text;
    }

    @Override
    public String render() {
        return "<input type=\"submit\" value=\""+text+"\" >";
    }
}
