package de.code.cloud95.spygame.page.entity;

public class Div implements Renderable{
    private final Renderable[] renderables;

    public Div(Renderable...renderables){
        this.renderables = renderables;
    }

    @Override
    public String render() {
        return "<div>"+render(renderables)+"</div>";
    }
}
