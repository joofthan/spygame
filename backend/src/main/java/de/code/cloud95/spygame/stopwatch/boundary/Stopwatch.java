package de.code.cloud95.spygame.stopwatch.boundary;

import de.code.cloud95.spygame.StatelessBoundary;
import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.entity.Game;
import de.code.cloud95.spygame.stopwatch.entity.Clock;
import de.code.cloud95.spygame.stopwatch.entity.Timer;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.Duration;

@StatelessBoundary
@NoArgsConstructor(force = true)
@RequiredArgsConstructor(onConstructor_ = @Inject)
public class Stopwatch {
    final UserService userService;
    final EntityManager em;
    final Clock clock;
    public Duration remainingTime() {
        Long gameId = userService.getUser().gameId;
        var timer = em.find(Timer.class, gameId);
        if(timer == null){
            throw new RuntimeException("Timer with id "+ gameId + " does not exist");
        }
        Game game = em.find(Game.class, gameId);
        return  game.roundDuration.minus(timer.read(clock.now()));
    }

    public void startStop() {
        Long gameId = userService.getUser().gameId;
        var timer = em.find(Timer.class, gameId);
        if(timer == null){
            timer = new Timer(gameId);
        }
        timer.startStop(clock.now());
        em.merge(timer);
    }
}
