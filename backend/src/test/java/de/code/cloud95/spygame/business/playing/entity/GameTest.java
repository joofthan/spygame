package de.code.cloud95.spygame.business.playing.entity;

import de.code.cloud95.spygame.playing.entity.Game;
import de.code.cloud95.spygame.playing.entity.Player;
import iteractor.lib.TestEntityManager;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;

import java.time.Duration;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {
    EntityManager em = TestEntityManager.create();
    @Test
    void saveLoad(){
        Game g = em.merge(new Game(1, Duration.ofMinutes(2), List.of(new Player()), 1L));
        var game = em.find(Game.class, g.id);
        assertEquals(1, game.players.size());
    }
}