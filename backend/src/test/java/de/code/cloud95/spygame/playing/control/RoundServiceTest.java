package de.code.cloud95.spygame.playing.control;

import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.entity.Player;
import iteractor.lib.TestEntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

class RoundServiceTest {

    RoundService roundService;
    UserService userService;
    EntityManager em;

    @BeforeEach
    void init(){
        userService = Mockito.mock(UserService.class);
        em = TestEntityManager.create();
        roundService = new RoundService(userService, em);
    }

    @Test
    void findCurrentRound() {
        Player p = new Player();
        p.id = 1L;
        em.merge(p);
        Mockito.when(userService.getUser()).thenReturn(p);
    }

    @Test
    void findAll() {
    }
}