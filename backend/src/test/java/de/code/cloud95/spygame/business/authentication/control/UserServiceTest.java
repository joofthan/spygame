package de.code.cloud95.spygame.business.authentication.control;

import de.code.cloud95.spygame.authentication.control.TokenGenerator;
import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.entity.Player;
import iteractor.lib.TestEntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class UserServiceTest {
    EntityManager em = TestEntityManager.create();
    UserService userService;
    @BeforeEach
    void init(){
        var tg = mock(TokenGenerator.class);
        userService = new UserService(tg,em);
    }
    @Test
    void addPlayer(){
        em.getTransaction().begin();
        userService.addUser("my");
        em.getTransaction().commit();
        assertEquals("my", em.createQuery("from Player", Player.class).getResultList().get(0).name);
    }

}