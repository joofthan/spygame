package bdd;

import com.tngtech.jgiven.annotation.ExpectedScenarioState;
import de.code.cloud95.spygame.lobby.boundary.Lobby;
import de.code.cloud95.spygame.playing.boundary.Startplayer;
import de.code.cloud95.spygame.playing.entity.Player;
import org.mockito.Mockito;

public class LobbyAction extends BaseState<LobbyAction> {

    @ExpectedScenarioState
    Lobby lobby;
    @ExpectedScenarioState
    Startplayer startplayer;

    public LobbyAction user_joins_with_name(String name) {
        var token = lobby.join(name);
        return this;
    }

    public LobbyAction current_player_is(String name){
        return player(name);
    }



    public LobbyAction user_starts_game() {
        Mockito.when(tokenGenerator.getPlayerId()).thenReturn(em.createQuery("from Player",Player.class).getResultList().get(0).id);
        startplayer.startNewGame(1,1);
        return this;

    }
    public LobbyAction starts_game() {
        startplayer.startNewGame(1,1);
        return this;

    }

    public LobbyAction chooses_start_deck(long deckId) {
        startplayer.chooseStartDeck(deckId);
        return this;
    }
}
