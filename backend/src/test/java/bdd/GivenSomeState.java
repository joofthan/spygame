package bdd;

import com.tngtech.jgiven.annotation.*;
import de.code.cloud95.spygame.authentication.control.TokenGenerator;
import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.lobby.boundary.Lobby;
import de.code.cloud95.spygame.playing.boundary.Decks;
import de.code.cloud95.spygame.playing.boundary.Startplayer;
import org.mockito.Mockito;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;

public class GivenSomeState extends InitState<GivenSomeState>{

    @ProvidedScenarioState
    Lobby lobby;
    @ProvidedScenarioState
    Startplayer startplayer;
    @ProvidedScenarioState
    UserService userService;



    @BeforeStage
    void initAllErl(){
        tokenGenerator = Mockito.mock(TokenGenerator.class);
        Mockito.when(tokenGenerator.generateToken(anyString(), anyLong())).thenReturn("myToken");
        userService = new UserService(tokenGenerator, em);
        lobby = new Lobby(userService, em);
        startplayer = new Startplayer(userService,em, lobby,new Decks(userService,em));
    }

    public void empty_lobby() {

    }

    public void lobby_with_players(String...names) {
        for (String name:names) {
            lobby.join(name);
        }
    }
}
