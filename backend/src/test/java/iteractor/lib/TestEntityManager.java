package iteractor.lib;


import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.HashMap;

public class TestEntityManager {
    public static EntityManager create() {
        return Persistence.createEntityManagerFactory("IT").createEntityManager();
    }
}
