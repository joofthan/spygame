package iteractor.lib;

import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.entity.Player;

import javax.persistence.EntityManager;
import java.util.concurrent.Callable;

public class TestUserService extends UserService {
    private EntityManager em;
    private Callable<Player> callable;

    public TestUserService(EntityManager em, Callable<Player> callable) {
        this.em = em;
        this.callable = callable;
    }
        public Player getUser() {
            try {
                return callable.call();
            } catch (Exception e) {
                throw new RuntimeException();
            }
        }
        public String addUser(String name) {
            em.merge(new Player(name));
            return "testToken";
        }
}
