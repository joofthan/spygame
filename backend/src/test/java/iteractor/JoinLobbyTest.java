package iteractor;

import bdd.GivenSomeState;
import bdd.LobbyOutcome;
import bdd.LobbyAction;
import com.tngtech.jgiven.junit5.ScenarioTest;
import org.junit.jupiter.api.Test;

class JoinLobbyTest extends ScenarioTest<GivenSomeState, LobbyAction, LobbyOutcome> {
    @Test
    void should_beInLobby_when_userJoined(){
        given().empty_lobby();
        when().user_joins_with_name("Jonis");
        then().number_of_waiting_players_is(1)
                .and().the_first_waiting_player_is("Jonis");

    }

    @Test
    void should_returnUser_when_userJoined(){
        given().empty_lobby();
        when().user_joins_with_name("Jonis")
                .and().current_player_is("Jonis");
        then().current_player_name_is("Jonis");
    }

    @Test
    void should_haveNoWaitingUsers_when_gameIsStarted(){
        given().lobby_with_players("Joni", "Max", "Peter");
        when().user_starts_game();
        then().number_of_waiting_players_is(0);
    }

    @Test
    void should_throw_when_userIsNotStartplayer(){
        given().lobby_with_players("Joni","Max");
        when().player("Max").starts_game();
        then().player("Joni").can_not_view_decks();
    }

    @Test
    void should_listDecks_when_userIsStartplayer(){
        given().lobby_with_players("Joni", "Max");
        when().player("Joni").starts_game();
        then().player("Joni").can_view_all_decks();
    }

    @Test
    void should_start_when_userIsStartplayer(){
        given().lobby_with_players("Joni", "Max");
        when().player("Joni").starts_game()
                .and().chooses_start_deck(1L);
        then().number_of_active_games_is(1)
                .and().first_game_has_deck(1L);
    }

    @Test
    void should_haveNoActiveGames_when_userHasNotStartedGame(){
        given().lobby_with_players("Joni", "Max");
        then().number_of_active_games_is(0);
    }
}