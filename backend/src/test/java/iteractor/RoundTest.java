package iteractor;

import de.code.cloud95.spygame.authentication.control.UserService;
import de.code.cloud95.spygame.playing.boundary.Decks;
import de.code.cloud95.spygame.playing.boundary.Hand;
import de.code.cloud95.spygame.playing.boundary.Startplayer;
import de.code.cloud95.spygame.playing.entity.Player;
import de.code.cloud95.spygame.lobby.boundary.Lobby;
import iteractor.lib.TestEntityManager;
import iteractor.lib.TestUserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.mock;

class RoundTest {


    Lobby lobby;
    Decks decks;
    Hand hand;
    Startplayer startplayer;
    UserService userService;

    EntityManager em;

    Player player1;
    Player player2;
    Player currentPlayer;

    @BeforeEach
    void init(){
        em = TestEntityManager.create();
        userService = new TestUserService(em,()->currentPlayer);
        lobby = new Lobby(userService, em);
        decks = new Decks(userService, em);
        hand = new Hand(userService, decks);
        startplayer = new Startplayer(userService, em, lobby, decks);

        em.getTransaction().begin();
        player1 = addPlayer("joni");
        player2 = addPlayer("max");
        currentPlayer = player1;
        startplayer.startNewGame(1,1);
        startplayer.chooseStartDeck(1L);
        em.getTransaction().commit();

        em.getTransaction().begin();
    }

    private Player addPlayer(String name) {
        userService.addUser(name);
        var all = em.createQuery("from Player", Player.class).getResultList();
        return all.get(all.size()-1);
    }

    @AfterEach
    void commit(){
        em.getTransaction().commit();
    }

    @Test
    void should_haveDifferentCards_when_differentUsersAreInOneRound(){
        currentPlayer = player1;
        var c1 = hand.viewCard();
        var c1Second = hand.viewCard();
        assertEquals(c1.cardId,c1Second.cardId);

        currentPlayer = player2;
        var c2 = hand.viewCard();

        assertNotEquals(c1,  c2);
    }

}