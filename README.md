# Full Stack
## Run
Run Backend and Frontend in paralell:  
`npm run dev`  

## Setup  
`npm i && cd frontend && npm i && cd ..`

---
### Backend  
`cd backend`  
`mvn quarkus:dev`  
[Quarkus Readme](https://gitlab.com/joofthan/templates/quarkus-minimal-pom/-/blob/master/README.md)
### Frontend  
`cd frontend`  
`npm run dev`  
[Svelte Readme](https://github.com/sveltejs/template/blob/master/README.md)